// .eleventy.js
module.exports = function(eleventyConfig) {

  // use a subdirectory, it’ll copy using the same directory structure.
  eleventyConfig.addPassthroughCopy("www/assets");

  return {
    dir: {
      input: "www",
      output: "dist"
    },
    passthroughFileCopy: true
  };
};
