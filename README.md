Candlewaster Website
====================
This is the website for [candlewaster.co](http://candlewaster.co).
It's a static site built with [11ty](https://www.11ty.io/).


Local Development
=================
```
npm install
npm run start
```

License
=======
CC-BY-SA 4.0. Attribution is not required, but attributing to "Candlewaster" would be appreciated. You may also use GNU GPL 2.0 or 3.0.
